<?php
/**
 * Created by DENSE.
 * User: Marek Jasan
 * Date: 14.4.2016
 * Time: 21:51
 */

return [
    'sk' => [
        'intl' => 'sk_SK',
        'name' => 'slovensky',
    ],
    'en' => [
        'intl' => 'en_US',
        'name' => 'english',
    ],
];
