<?php

namespace Dense\Localization\Providers;

use Illuminate\Support\ServiceProvider;

class LocalizationServiceProvider extends ServiceProvider
{
    protected $defer = false;

    public function boot()
    {
        $configDir = __DIR__ . '/../../config';

        $this->mergeConfigFrom($configDir . '/languages.php', 'languages');

        $this->mergeConfigFrom($configDir . '/localization.php', 'localization');
    }

    public function register()
    {
        $configDir = __DIR__ . '/../../config';

        $this->publishes([
            $configDir . '/localization.php' => config_path('localization.php')
        ], 'config');
    }
}
