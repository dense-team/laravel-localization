<?php

namespace Dense\Localization\Middleware;

class FallbackLangRedirect
{
    public function handle($request, \Closure $next)
    {
        $segmentCheck = strlen(\Request::segment(1));

        if ($segmentCheck === 2) {
            $lang = lang();
            $defaultLang = default_lang();

            if ($lang === $defaultLang) {
                $uri = trim(substr(\Request::path(), 2), '/');

                return redirect($uri);
            }
        }

        return $next($request);
    }
}
