<?php
/**
 * Created by DENSE.
 * User: Marek Jasan
 * Date: 9.3.2016
 * Time: 23:42
 */

namespace Dense\Localization\Middleware;

class LocaleDetect
{
    public function handle($request, \Closure $next)
    {
        if (\Config::get('localization.force_locale') === false) {
            $lang = $request->segment(1);

            $allowedLangs = locales();

            if (!in_array($lang, $allowedLangs)) {
                $lang = default_lang();
            }
        } else {
            $lang = default_lang();
        }

        \App::setLocale($lang);

        return $next($request);
    }
}
