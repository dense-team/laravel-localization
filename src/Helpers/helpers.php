<?php

if (!function_exists('host')) {

    function host()
    {
        /*
        list($protocol, $host) = explode('://', config('app.url'));

        if (isset($_SERVER['HTTP_HOST'])) {
            $host = $_SERVER['HTTP_HOST'];
        }

        if (isset($_SERVER['SERVER_PROTOCOL'])) {
            $protocol = $_SERVER['SERVER_PROTOCOL'];
        }

        return $protocol . '://' . $host;
        */

        return config('app.url');
    }
}

if (!function_exists('locales')) {

    function locales()
    {
        $configLocalizations = config('localization.locales');
        return $configLocalizations;
    }
}

if (!function_exists('langs')) {

    function langs()
    {
        $configLangs = config('languages');
        return $configLangs;
    }
}

if (!function_exists('lang')) {

    function lang()
    {
        return \App::getLocale();
    }
}

if (!function_exists('default_lang')) {

    function default_lang()
    {
        return config('app.locale');
    }
}

if (!function_exists('fallback_lang')) {

    function fallback_lang()
    {
        return config('app.fallback_locale');
    }
}

if (!function_exists('lang_intl')) {

    function lang_intl($lang = null)
    {
        $configLangs = langs();

        if (is_null($lang)) {
            $lang = lang();
        }

        return $configLangs[$lang]['intl'];
    }
}

if (!function_exists('lang_name')) {

    function lang_name($lang = null)
    {
        $configLangs = langs();

        if (is_null($lang)) {
            $lang = lang();
        }

        return $configLangs[$lang]['name'];
    }
}

if (!function_exists('url_lang')) {

    function url_lang($lang = null)
    {
        $defaultLang = default_lang();

        if (is_null($lang)) {
            $lang = lang();
        }

        if ($defaultLang !== $lang) {
            return $lang;
        }
    }
}

if (!function_exists('route_lang')) {

    function route_lang($name, $parameters = [], $absolute = false)
    {
        $lang = url_lang();
        $uri = trim(route($name, $parameters, false), '/');

        $url = '/' . trim($lang . '/' . $uri, '/');

        if ($absolute === true) {
            $url = host() . $url;
        }

        return $url;
    }
}
